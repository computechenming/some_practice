﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Animal
{
    public abstract class Animal
    {
        public string AnimalName { get; set; }
        public string AmimalColor { get; set; }
        public virtual void Mobile()
        {
            Console.WriteLine("移动");
        }
    }
}
